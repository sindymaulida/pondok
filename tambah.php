<?php include('config.php'); ?>
<!DOCTYPE html>
<html>
<head>
	<title>PONDOK IT</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container">
			<a class="navbar-brand" href="#">MUSLIMAH</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
 
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link" href="index.php">Home</a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="tambah.php">Tambah</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	
	<div class="container" style="margin-top:20px">
		<h2>Tambah Santri</h2>
		
		<hr>
		
		<?php
		if(isset($_POST['submit'])){
			$nama_santri			= $_POST['nama_santri'];
			$jenis_kelamin		= $_POST['jenis_kelamin'];
			$jurusan	= $_POST['jurusan'];
			$asal		= $_POST['asal'];
			
			$cek = mysqli_query($koneksi, "SELECT * FROM Muslimah WHERE nama_santri='$nama_santri'") or die(mysqli_error($koneksi));
			
			if(mysqli_num_rows($cek) == 0){
				$sql = mysqli_query($koneksi, "INSERT INTO Muslimah(nama_santri, jenis_kelamin, jurusan, asal) VALUES('$nama_santri', '$jens_kelamin', '$jurusan', '$asal')") or die(mysqli_error($koneksi));
				
				if($sql){
					echo '<script>alert("Berhasil menambahkan data."); document.location="tambah.php";</script>';
				}else{
					echo '<div class="alert alert-warning">Gagal melakukan proses tambah data.</div>';
				}
			}else{
				echo '<div class="alert alert-warning">Gagal, NAMA sudah terdaftar.</div>';
			}
		}
		?>
		
		<form action="tambah.php" method="post">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">NAMA SANTRI</label>
				<div class="col-sm-10">
					<input type="text" name="nama santri" class="form-control" size="4" required>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">JENIS KELAMIN</label>
				<div class="col-sm-10">
					<div class="form-check">
						<input type="radio" class="form-check-input" name="jenis_kelamin" value="LAKI-LAKI" required>
						<label class="form-check-label">LAKI-LAKI</label>
					</div>
					<div class="form-check">
						<input type="radio" class="form-check-input" name="jenis_kelamin" value="PEREMPUAN" required>
						<label class="form-check-label">PEREMPUAN</label>
					    </div>
					</div>
				</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">JURUSAN</label>
				<div class="col-sm-10">
					<select name="jurusan" class="form-control" required>
						<option value="">PILIH JURUSAN</option>
						<option value="MULTIMEDIA">MULTIMEDIA</option>
						<option value="IMERS">IMERS</option>
						<option value="PROGRAMMER">PROGRAMMER</option>
						<option value="KOKI">KOKI</option>
						<option value="MANAJEMEN">MANAJEMEN</option>
					</select>
				</div>
			</div>
		<div class="form-group row">
				<label class="col-sm-2 col-form-label">ASAL</label>
				<div class="col-sm-10">
					<select name="asal" class="form-control" required>
						<option value="">PILIH ASAL</option>
						<option value="PADANG SUMATRA BARAT">PADANG SUMATRA BARAT</option>
						<option value="PEKALONGAN">PEKALONGAN</option>
						<option value="JAKARTA">JAKARTA</option>
						<option value="PALEMBANG">PALEMBANGK</option>
						<option value="BANJARNEGARA">BANJARNEGARA</option>
						<option value="YOGYAKARTA">YOGYAKARTA</option>
						<option value="BEKASI">BEKASI</option>
						<option value="SEMARANG">SEMARANG</option>
						<option value="SSALATIGA">SALATIGA</option>
						<option value="INDRAMAYU">INDRAMAYU</option>
						<option value="BATANG JAWA TENGAH">BATANG JAWA TENGAH</option>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">&nbsp;</label>
				<div class="col-sm-10">
					<input type="submit" name="submit" class="btn btn-primary" value="SIMPAN">
				</div>
			</div>
		</form>
		
	</div>
	
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	
</body>
</html>